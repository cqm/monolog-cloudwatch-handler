<?php

namespace Chequemotiva\Monolog\CloudWatch;

use Aws\CloudWatchLogs\CloudWatchLogsClient;
use Aws\CloudWatchLogs\Exception\CloudWatchLogsException;
use Chequemotiva\Monolog\CloudWatch\CloudWatchLogs\Client;
use Chequemotiva\Monolog\CloudWatch\CloudWatchLogs\Exceptions\DataAlreadyAcceptedException;
use Chequemotiva\Monolog\CloudWatch\CloudWatchLogs\Exceptions\InvalidSequenceTokenException;
use Chequemotiva\Monolog\CloudWatch\CloudWatchLogs\Exceptions\LogGroupNotFoundException;
use Chequemotiva\Monolog\CloudWatch\CloudWatchLogs\Exceptions\LogStreamNotFoundException;
use Chequemotiva\Monolog\CloudWatch\CloudWatchLogs\Exceptions\ResourceAlreadyExistsException;
use Chequemotiva\Monolog\CloudWatch\EventBuffers\DefaultEventBuffer;
use Chequemotiva\Monolog\CloudWatch\EventBuffers\EventBufferInterface;
use Chequemotiva\Monolog\CloudWatch\SequenceTokenRepositories\MemorySequenceTokenRepository;
use Chequemotiva\Monolog\CloudWatch\SequenceTokenRepositories\SequenceTokenRepositoryInterface;
use Monolog\Formatter\FormatterInterface;
use Monolog\Formatter\LineFormatter;
use Monolog\Handler\AbstractProcessingHandler;
use Monolog\Logger;

class CloudWatchHandler extends AbstractProcessingHandler
{

    private Client $client;
    private string $group;
    private string $stream;
    private SequenceTokenRepositoryInterface $sequenceTokenRepository;
    private EventBufferInterface $eventBuffer;

    /**
     * @param CloudWatchLogsClient $client
     *
     * @param string $group
     *  Log group names must be unique within a region for an AWS account.
     *  Log group names can be between 1 and 512 characters long.
     *  Log group names consist of the following characters: a-z, A-Z, 0-9, '_' (underscore), '-' (hyphen),
     * '/' (forward slash), and '.' (period).
     *
     * @param string $stream
     *  Log stream names must be unique within the log group.
     *  Log stream names can be between 1 and 512 characters long.
     *  The ':' (colon) and '*' (asterisk) characters are not allowed.
     *
     * @param string|SequenceTokenRepositoryInterface|null $sequenceToken
     * @param EventBufferInterface|null $eventBuffer
     * @param int|string $level
     * @param bool $bubble
     */
    public function __construct(
        CloudWatchLogsClient $client,
        string $group,
        string $stream,
        string|SequenceTokenRepositoryInterface|null $sequenceToken = null,
        EventBufferInterface|null $eventBuffer = null,
        int|string $level = Logger::DEBUG,
        bool $bubble = true,
    ) {
        $this->client = new Client($client);
        $this->group = $group;
        $this->stream = $stream;
        $this->sequenceTokenRepository = $this->initSequenceTokenRepository($group, $stream, $sequenceToken);
        $this->eventBuffer = $eventBuffer ?? new DefaultEventBuffer();

        parent::__construct($level, $bubble);
    }

    private function initSequenceTokenRepository(string $group, string $stream, SequenceTokenRepositoryInterface|string|null $sequenceToken): SequenceTokenRepositoryInterface
    {
        if ($sequenceToken instanceof SequenceTokenRepositoryInterface) {
            return $sequenceToken;
        }

        $repository = new MemorySequenceTokenRepository();
        if ($sequenceToken !== null) {
            $repository->setSequenceToken($group, $stream, $sequenceToken);
        }

        return $repository;
    }

    /**
     * {@inheritdoc}
     */
    protected function write(array $record): void
    {
        $event = Event::createFromRecord($record);

        if (!$this->eventBuffer->add($event)) {
            $this->flush();

            $this->eventBuffer->add($event) || throw new \RuntimeException("Stupid buffer");
        }

        if ($this->eventBuffer->isFull()) {
            $this->flush();
        }
    }


    /**
     * @throws LogStreamNotFoundException
     * @throws LogGroupNotFoundException
     * @throws InvalidSequenceTokenException
     * @throws DataAlreadyAcceptedException
     * @throws CloudWatchLogsException
     */
    public function flush(): void
    {
        try {
            $this->send();
        } catch (InvalidSequenceTokenException|DataAlreadyAcceptedException $ex) {
            $this->sequenceTokenRepository->setSequenceToken($this->group, $this->stream, $ex->getExpectedSequenceToken());
            $this->send();
        } catch (LogStreamNotFoundException) {
            $this->createLogStream();
            $this->flush();
        }
    }

    /**
     * @throws DataAlreadyAcceptedException
     * @throws InvalidSequenceTokenException
     * @throws LogStreamNotFoundException
     * @throws LogGroupNotFoundException
     * @throws CloudWatchLogsException
     */
    private function send(): void
    {
        if ($this->eventBuffer->isEmpty()) {
            return;
        }

        // Take events from buffer
        $events = $this->eventBuffer->all();

        // Convert to array
        $events = array_map(static fn(Event $event) => $event->toArray(), $events);

        // Sort events (AWS expects to receive entries in chronological order)
        usort($events, static fn (array $a, array $b) => $a['timestamp'] <=> $b['timestamp']);

        // Send events
        $response = $this->client->putLogEvents(
            $events,
            $this->group,
            $this->stream,
            $this->sequenceTokenRepository->getSequenceToken($this->group, $this->stream)
        );

        // Update sequence token
        $this->sequenceTokenRepository->setSequenceToken($this->group, $this->stream, $response->get('nextSequenceToken'));

        // Clear buffer
        $this->eventBuffer->clear();
    }

    private function createLogStream(): void
    {
        try {
            $this->client->createLogStream($this->group, $this->stream);
        } catch (ResourceAlreadyExistsException) {
            // Do nothing
        }
    }

    /**
     * {@inheritdoc}
     */
    protected function getDefaultFormatter(): FormatterInterface
    {
        return new LineFormatter("%channel%: %level_name%: %message% %context% %extra%", null, false, true);
    }

    /**
     * {@inheritdoc}
     */
    public function close(): void
    {
        $this->flush();

        parent::close();
    }

    /**
     * {@inheritdoc}
     */
    public function reset()
    {
        $this->flush();

        parent::reset();
    }


}
