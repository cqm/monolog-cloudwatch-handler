<?php

namespace Chequemotiva\Monolog\CloudWatch\EventBuffers;

/**
 * Buffer that send events when a certain threshold of messages is reached.
 *
 * The default event buffer can collect up to 10.000 events; this event buffer can send events in smaller batches,
 * event one by one if threshold is set to 1.
 */
class OverflowEventBuffer extends DefaultEventBuffer
{

    private int $threshold;

    public function __construct(int $threshold)
    {
        $this->setThreshold($threshold);
    }

    public function getThreshold(): int
    {
        return $this->threshold;
    }

    public function setThreshold(int $threshold): void
    {
        if ($threshold < 1) {
            throw new \RangeException("Threshold must be greater or equals to 1");
        } elseif ($threshold > self::MAX_EVENT_COUNT) {
            throw new \RangeException("Threshold can not be greater than ". self::MAX_EVENT_COUNT);
        }

        $this->threshold = $threshold;
    }

    public function isFull(): bool
    {
        return parent::isFull() || $this->count() >= $this->threshold;
    }

}
