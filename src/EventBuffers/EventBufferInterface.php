<?php

namespace Chequemotiva\Monolog\CloudWatch\EventBuffers;

use Chequemotiva\Monolog\CloudWatch\Event;

interface EventBufferInterface
{

    public function add(Event $event): bool;
    public function isEmpty(): bool;
    public function isFull(): bool;
    public function count(): int;
    /** @return array<Event> */
    public function all(): array;
    public function clear(): void;

}