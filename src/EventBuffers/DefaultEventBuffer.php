<?php

namespace Chequemotiva\Monolog\CloudWatch\EventBuffers;

use Chequemotiva\Monolog\CloudWatch\Event;

/*
 * The batch of events must satisfy the following constraints:
 *  - The maximum batch size is 1,048,576 bytes, and this size is calculated as the sum of all event messages in
 * UTF-8, plus 26 bytes for each log event.
 *  - None of the log events in the batch can be more than 2 hours in the future.
 *  - None of the log events in the batch can be older than 14 days or the retention period of the log group.
 *  - The log events in the batch must be in chronological ordered by their timestamp (the time the event occurred,
 * expressed as the number of milliseconds since Jan 1, 1970 00:00:00 UTC).
 *  - The maximum number of log events in a batch is 10,000.
 *  - A batch of log events in a single request cannot span more than 24 hours. Otherwise, the operation fails.
 */
class DefaultEventBuffer implements EventBufferInterface
{

    /**
     * Event size limit (https://docs.aws.amazon.com/AmazonCloudWatch/latest/logs/cloudwatch_limits_cwl.html)
     */
    const EVENT_SIZE_LIMIT = 262_144; // 256 KiB

    /**
     * Data amount limit (http://docs.aws.amazon.com/AmazonCloudWatchLogs/latest/APIReference/API_PutLogEvents.html)
     */
    const MAX_BATCH_SIZE = 1_048_576; // 1 MiB

    /**
     * Event count limit (http://docs.aws.amazon.com/AmazonCloudWatchLogs/latest/APIReference/API_PutLogEvents.html)
     */
    const MAX_EVENT_COUNT = 10_000;

    /**
     * Bytes added to each event message (https://docs.aws.amazon.com/AmazonCloudWatchLogs/latest/APIReference/API_PutLogEvents.html)
     */
    const EVENT_ADDED_BYTES = 26;

    /**
     * Maximum size of each event chunk
     */
    const MAX_EVENT_SIZE = self::EVENT_SIZE_LIMIT - self::EVENT_ADDED_BYTES; // 262.118

    /**
     * @var array<Event>
     */
    private array $events = [];
    private int $batchSize = 0;

    /**
     * @param Event $event
     * @return bool `true` if the event was added to the batch, `false` it the event does not fit in
     */
    public function add(Event $event): bool
    {
        // Is full?
        if ($this->isFull()) {
            return false;
        }

        // Check total size
        $eventSize = $this->getEventSize($event);
        if ($this->batchSize + $eventSize > self::MAX_BATCH_SIZE) {
            return false;
        }

        // Check event count
        $eventChunkCount = $this->countEventChunks($event);
        if (count($this->events) + $eventChunkCount > self::MAX_EVENT_COUNT) {
            return false;
        }

        $this->events = array_merge($this->events, $this->splitEvent($event));
        $this->batchSize += $eventSize;

        return true;
    }

    public function count(): int
    {
        return count($this->events);
    }

    public function isEmpty(): bool
    {
        return $this->batchSize === 0;
    }

    public function isFull(): bool
    {
        return
            $this->batchSize >= (self::MAX_BATCH_SIZE - self::EVENT_ADDED_BYTES)
            ||
            count($this->events) >= self::MAX_EVENT_COUNT;
    }

    public function all(): array
    {
        return $this->events;
    }

    public function clear(): void
    {
        $this->events = [];
        $this->batchSize = 0;
    }

    private function countEventChunks(Event $event): int
    {
        return ceil(strlen($event->getMessage()) / self::MAX_EVENT_SIZE);
    }

    private function getEventSize(Event $event): int
    {
        $length = strlen($event->getMessage());
        $chunks = $this->countEventChunks($event);

        return $length + ($chunks * self::EVENT_ADDED_BYTES);
    }

    /**
     * @return array<Event>
     */
    private function splitEvent(Event $event): array
    {
        if (strlen($event->getMessage()) < self::MAX_EVENT_SIZE) {
            return [$event];
        }

        $events = [];
        $chunks = str_split($event->getMessage(), self::MAX_EVENT_SIZE);

        foreach ($chunks as $chunk) {
            $events[] = new Event($chunk, $event->getTimestamp());
        }

        return $events;
    }

}
