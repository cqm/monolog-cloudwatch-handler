<?php

namespace Chequemotiva\Monolog\CloudWatch;

class Event
{

    private string $message;
    private float $timestamp;

    public function __construct(string $message, float $timestamp)
    {
        $this->message = $message;
        $this->timestamp = $timestamp;
    }

    public function getMessage(): string
    {
        return $this->message;
    }

    public function setMessage(string $message): void
    {
        $this->message = $message;
    }

    public function getTimestamp(): float
    {
        return $this->timestamp;
    }

    public function setTimestamp(float $timestamp): void
    {
        $this->timestamp = $timestamp;
    }

    public function toArray(): array
    {
        return [
            'message' => $this->message,
            'timestamp' => (int)$this->timestamp,
        ];
    }

    static public function createFromRecord(array $record): self
    {
        return new self(
            $record['formatted'],
            $record['datetime']->format('U.u') * 1000,
        );
    }

}