<?php

namespace Chequemotiva\Monolog\CloudWatch\SequenceTokenRepositories;

interface SequenceTokenRepositoryInterface
{

    public function getSequenceToken(string $group, string $stream): string|null;
    public function setSequenceToken(string $group, string $stream, string|null $sequenceToken): void;

}