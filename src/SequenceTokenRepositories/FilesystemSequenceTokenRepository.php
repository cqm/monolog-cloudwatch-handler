<?php

namespace Chequemotiva\Monolog\CloudWatch\SequenceTokenRepositories;

class FilesystemSequenceTokenRepository implements SequenceTokenRepositoryInterface
{

    private string $path;

    public function __construct(string $path)
    {
        if (!file_exists($path) && !@mkdir($path)) {
            throw new \RuntimeException('Path "'. $path .'" does not exists');
        }

        if (!is_dir($path)) {
            throw new \RuntimeException('Path "'. $path .'" is not a directory');
        }

        if (!is_writable($path)) {
            throw new \RuntimeException('Path "'. $path .'" is not writable');
        }

        if (!str_ends_with($path, DIRECTORY_SEPARATOR)) {
            $path .= DIRECTORY_SEPARATOR;
        }

        $this->path = $path;
    }

    public function getSequenceToken(string $group, string $stream): ?string
    {
        $file = $this->getFileName($group, $stream);

        if (!is_file($file) || !is_readable($file)) {
            return null;
        }

        return file_get_contents($file);
    }

    public function setSequenceToken(string $group, string $stream, string|null $sequenceToken): void
    {
        file_put_contents($this->getFileName($group, $stream), $sequenceToken);
    }

    private function getFileName(string $group, string $stream): string
    {
        return $this->path . md5($group . ' ' . $stream);
    }

}
