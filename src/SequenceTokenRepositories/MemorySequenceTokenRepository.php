<?php

namespace Chequemotiva\Monolog\CloudWatch\SequenceTokenRepositories;

class MemorySequenceTokenRepository implements SequenceTokenRepositoryInterface
{

    private array $sequenceTokens = [];

    public function getSequenceToken(string $group, string $stream): ?string
    {
        return $this->sequenceTokens[$group][$stream] ?? null;
    }

    public function setSequenceToken(string $group, string $stream, string|null $sequenceToken): void
    {
        $this->sequenceTokens[$group][$stream] = $sequenceToken;
    }

}
