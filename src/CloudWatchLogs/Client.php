<?php

namespace Chequemotiva\Monolog\CloudWatch\CloudWatchLogs;

use Aws\CloudWatchLogs\CloudWatchLogsClient;
use Aws\CloudWatchLogs\Exception\CloudWatchLogsException;
use Aws\Result;
use Chequemotiva\Monolog\CloudWatch\CloudWatchLogs\Exceptions\DataAlreadyAcceptedException;
use Chequemotiva\Monolog\CloudWatch\CloudWatchLogs\Exceptions\InvalidSequenceTokenException;
use Chequemotiva\Monolog\CloudWatch\CloudWatchLogs\Exceptions\LogGroupNotFoundException;
use Chequemotiva\Monolog\CloudWatch\CloudWatchLogs\Exceptions\LogStreamNotFoundException;
use Chequemotiva\Monolog\CloudWatch\CloudWatchLogs\Exceptions\ResourceAlreadyExistsException;

class Client
{

    private CloudWatchLogsClient $client;

    public function __construct(CloudWatchLogsClient $client)
    {
        $this->client = $client;
    }

    /**
     * @throws ResourceAlreadyExistsException
     * @throws CloudWatchLogsException
     */
    public function createLogStream(string $group, string $stream): void
    {
        try {
            $this->client->createLogStream([
                'logGroupName' => $group,
                'logStreamName' => $stream,
            ]);
        } catch (CloudWatchLogsException $ex) {
            throw match ($ex->getAwsErrorCode()) {
                'ResourceAlreadyExistsException' => new ResourceAlreadyExistsException($ex->getAwsErrorMessage(), 0, $ex),
                default => $ex,
            };
        }
    }

    /**
     * @throws InvalidSequenceTokenException
     * @throws DataAlreadyAcceptedException
     * @throws LogStreamNotFoundException
     * @throws LogGroupNotFoundException
     * @throws CloudWatchLogsException
     */
    public function putLogEvents(array $events, string $group, string $stream, ?string $secuenceToken = null): Result
    {
        try {
            return $this->client->putLogEvents(array_filter([
                'logGroupName' => $group,
                'logStreamName' => $stream,
                'logEvents' => $events,
                'sequenceToken' => $secuenceToken,
            ]));
        } catch (CloudWatchLogsException $ex) {
            throw match ($ex->getAwsErrorCode()) {
                'InvalidSequenceTokenException' => new InvalidSequenceTokenException($ex['expectedSequenceToken'], $ex->getAwsErrorMessage(), 0, $ex),
                'DataAlreadyAcceptedException' => new DataAlreadyAcceptedException($ex['expectedSequenceToken'], $ex->getAwsErrorMessage(), 0, $ex),
                'ResourceNotFoundException' =>
                    str_contains($ex->getAwsErrorMessage(), 'stream')
                    ? new LogStreamNotFoundException($stream, $ex->getAwsErrorMessage(), 0, $ex)
                    : new LogGroupNotFoundException($group, $ex->getAwsErrorMessage(), 0, $ex),
                default => $ex,
            };
        }
    }

}
