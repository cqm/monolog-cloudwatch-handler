<?php

namespace Chequemotiva\Monolog\CloudWatch\CloudWatchLogs\Exceptions;

class InvalidSequenceTokenException extends \Exception
{

    private string|null $expectedSequenceToken;

    public function __construct(string|null $expectedSequenceToken, string $message = '', int $code = 0, \Throwable|null $previous = null)
    {
        parent::__construct($message, $code, $previous);

        $this->expectedSequenceToken = $expectedSequenceToken;
    }

    public function getExpectedSequenceToken(): string|null
    {
        return $this->expectedSequenceToken;
    }

}
