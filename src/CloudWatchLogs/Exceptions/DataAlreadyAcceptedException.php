<?php

namespace Chequemotiva\Monolog\CloudWatch\CloudWatchLogs\Exceptions;

class DataAlreadyAcceptedException extends \Exception
{

    private string $expectedSequenceToken;

    public function __construct(string $expectedSequenceToken, string $message = '', int $code = 0, \Throwable|null $previous = null)
    {
        parent::__construct($message, $code, $previous);

        $this->expectedSequenceToken = $expectedSequenceToken;
    }

    public function getExpectedSequenceToken(): string
    {
        return $this->expectedSequenceToken;
    }

}
