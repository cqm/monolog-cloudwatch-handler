<?php

namespace Chequemotiva\Monolog\CloudWatch\CloudWatchLogs\Exceptions;

class LogGroupNotFoundException extends \Exception
{

    private string $logStream;

    public function __construct(string $logStream, string $message = '', int $code = 0, \Throwable|null $previous = null)
    {
        parent::__construct($message, $code, $previous);

        $this->logStream = $logStream;
    }

    public function getLogStream(): string
    {
        return $this->logStream;
    }

}
