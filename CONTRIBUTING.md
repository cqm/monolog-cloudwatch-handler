## How to contribute

#### **Did you find a bug?**

* **Ensure the bug was not already reported** by searching on Bitbucket under [Issues](https://bitbucket.org/cqm/monolog-cloudwatch-handler/issues).

* If you're unable to find an open issue addressing the problem, [open a new one](https://bitbucket.org/cqm/monolog-cloudwatch-handler/issues/new). Be sure to include a **title and clear description**, as much relevant information as possible, and a **code sample** or an **executable test case** demonstrating the expected behavior that is not occurring.

#### **Did you write a patch that fixes a bug?**

* Open a new Bitbucket pull request with the patch.

* Ensure the PR description clearly describes the problem and solution. Include the relevant issue number if applicable.

#### **Did you fix whitespace, format code, or make a purely cosmetic patch?**

Changes that are cosmetic in nature and do not add anything substantial to the stability, functionality, or testability will generally not be accepted.

Thanks!
